public class Dashline {
  public static void main(String[] args) {
    System.out.println("12 dash line");
    System.out.println("-" * 12);
  }
}

/*EXPECT
12 dash line
------------
*/
