// Using a variable to decide the number of repeats.
public class StringRepVar {
  public static void main(String[] args) {
    int num = 0;
    for (int i = 1; i <= 4; i += 1)
      num += i;
    System.out.println("/" * num); // Prints 10x forward slash.
  }
}

/*EXPECT
//////////
*/
